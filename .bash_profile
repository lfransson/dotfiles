# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

export PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH=$HOME/.rbenv/bin:$PATH

if [ -f ~/.rbenv/bin/rbenv ]; then
  eval "$(rbenv init -)"
fi
